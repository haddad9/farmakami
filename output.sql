--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Ubuntu 12.2-2.pgdg16.04+1)
-- Dumped by pg_dump version 12.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: farmakami; Type: SCHEMA; Schema: -; Owner: itxnrcnocpagke
--

CREATE SCHEMA farmakami;


ALTER SCHEMA farmakami OWNER TO itxnrcnocpagke;

--
-- Name: assign_apoteker(); Type: FUNCTION; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE FUNCTION farmakami.assign_apoteker() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
flag_status BOOLEAN;
flag_apoteker BOOLEAN;
BEGIN
IF(TG_OP = 'UPDATE') THEN
IF(OLD.status_validasi = 'Processing' OR OLD.status_validasi = 'Done') THEN
RAISE EXCEPTION 'Maaf, resep ini sudah diproses oleh apoteker';
ELSIF(NEW.status_validasi = 'Wait') THEN
IF(NEW.email_apoteker IS NOT NULL) THEN
RAISE EXCEPTION 'Resep sudah dapat diproses. Ubah status validasi menjadi "Processing"';
ELSE
RAISE EXCEPTION 'Tidak terjadi perubahan';
END IF;
ELSE
RETURN NEW;
END IF;
ELSIF(TG_OP = 'INSERT') THEN
flag_apoteker := NEW.email_apoteker IS NULL;
flag_status := NEW.status_validasi = 'Wait';
IF(flag_apoteker = FALSE) THEN
IF(flag_status = TRUE) THEN
RAISE EXCEPTION 'Resep sudah dapat diproses. Ubah status validasi menjadi "Processing"';
ELSE
RETURN NEW;
END IF;
ELSE
IF(flag_status = TRUE) THEN
RETURN NEW;
ELSE
RAISE EXCEPTION 'Maaf, resep harus menunggu terlebih dahulu apabila belum ada apoteker yang akan memproses. Ubah status validasi menjadi "Wait"';
END IF;
END IF;
END IF;
END;
$$;


ALTER FUNCTION farmakami.assign_apoteker() OWNER TO itxnrcnocpagke;

--
-- Name: pembuatan_pengantaran_farmasi(); Type: FUNCTION; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE FUNCTION farmakami.pembuatan_pengantaran_farmasi() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        total_biaya_temp INTEGER;
        temp_row RECORD;
    BEGIN
        FOR temp_row IN SELECT * FROM PENGANTARAN_FARMASI
        LOOP
            SELECT (temp_row.biaya_kirim + t.total_pembayaran) INTO total_biaya_temp
            FROM PENGANTARAN_FARMASI p, TRANSAKSI_PEMBELIAN t
            WHERE p.id_pengantaran = temp_row.id_pengantaran
            AND temp_row.id_transaksi_pembelian = t.id_transaksi_pembelian;

            UPDATE PENGANTARAN_FARMASI
            SET total_biaya = total_biaya_temp
            WHERE temp_row.id_pengantaran = id_pengantaran;
        END LOOP;
        RETURN NEW;
    END;
$$;


ALTER FUNCTION farmakami.pembuatan_pengantaran_farmasi() OWNER TO itxnrcnocpagke;

--
-- Name: update_total_pembayaran(); Type: FUNCTION; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE FUNCTION farmakami.update_total_pembayaran() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 DECLARE
    temp_bayar INTEGER;
    temp_jumlah INTEGER;
    temp_harga_jual INTEGER;
    temp_row RECORD;
 BEGIN

    Update transaksi_pembelian Set total_pembayaran = 0 ;

    For temp_row in
        Select * from list_produk_dibeli
    Loop
        temp_bayar := 0;
        select harga_jual into temp_harga_jual from produk_apotek where id_produk = temp_row.id_produk and id_apotek = temp_row.id_apotek;
        select jumlah into temp_jumlah from list_produk_dibeli where id_produk = temp_row.id_produk;
        temp_bayar := temp_bayar + (temp_harga_jual*temp_jumlah);
        
        Update transaksi_pembelian
        Set total_pembayaran = total_pembayaran + temp_bayar
        Where id_transaksi_pembelian = temp_row.id_transaksi_pembelian;

        temp_bayar := 0;

    End loop;
    return new;
 END;
$$;


ALTER FUNCTION farmakami.update_total_pembayaran() OWNER TO itxnrcnocpagke;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin_apotek; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.admin_apotek (
    email character varying(50) NOT NULL,
    id_apotek character varying(10)
);


ALTER TABLE farmakami.admin_apotek OWNER TO itxnrcnocpagke;

--
-- Name: alamat_konsumen; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.alamat_konsumen (
    id_konsumen character varying(10) NOT NULL,
    alamat text NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE farmakami.alamat_konsumen OWNER TO itxnrcnocpagke;

--
-- Name: alat_medis; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.alat_medis (
    id_alat_medis character varying(10) NOT NULL,
    nama_alat_medis character varying(50) NOT NULL,
    deskripsi_alat text,
    jenis_penggunaan character varying(20) NOT NULL,
    id_produk character varying(10) NOT NULL
);


ALTER TABLE farmakami.alat_medis OWNER TO itxnrcnocpagke;

--
-- Name: apotek; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.apotek (
    id_apotek character varying(10) NOT NULL,
    email character varying(50) NOT NULL,
    no_sia character varying(20) NOT NULL,
    nama_penyelenggara character varying(50) NOT NULL,
    nama_apotek character varying(50) NOT NULL,
    alamat_apotek text NOT NULL,
    telepon_apotek character varying(20)
);


ALTER TABLE farmakami.apotek OWNER TO itxnrcnocpagke;

--
-- Name: apoteker; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.apoteker (
    email character varying(50) NOT NULL
);


ALTER TABLE farmakami.apoteker OWNER TO itxnrcnocpagke;

--
-- Name: balai_apotek; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.balai_apotek (
    id_balai character varying(10) NOT NULL,
    id_apotek character varying(10) NOT NULL
);


ALTER TABLE farmakami.balai_apotek OWNER TO itxnrcnocpagke;

--
-- Name: balai_pengobatan; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.balai_pengobatan (
    id_balai character varying(10) NOT NULL,
    alamat_balai text NOT NULL,
    nama_balai character varying(50) NOT NULL,
    jenis_balai character varying(30) NOT NULL,
    telepon_balai character varying(20)
);


ALTER TABLE farmakami.balai_pengobatan OWNER TO itxnrcnocpagke;

--
-- Name: cs; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.cs (
    no_ktp character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    no_sia character varying(20) NOT NULL
);


ALTER TABLE farmakami.cs OWNER TO itxnrcnocpagke;

--
-- Name: kandungan; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.kandungan (
    id_bahan character varying(10) NOT NULL,
    id_obat character varying(10) NOT NULL,
    takaran character varying(10) NOT NULL,
    satuan_takar character varying(10)
);


ALTER TABLE farmakami.kandungan OWNER TO itxnrcnocpagke;

--
-- Name: kategori_merk_obat; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.kategori_merk_obat (
    id_kategori character varying(10) NOT NULL,
    id_merk_obat character varying(10) NOT NULL
);


ALTER TABLE farmakami.kategori_merk_obat OWNER TO itxnrcnocpagke;

--
-- Name: kategori_obat; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.kategori_obat (
    id_kategori character varying(10) NOT NULL,
    nama_kategori character varying(50) NOT NULL,
    deskripsi_kategori text
);


ALTER TABLE farmakami.kategori_obat OWNER TO itxnrcnocpagke;

--
-- Name: komposisi; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.komposisi (
    id_bahan character varying(10) NOT NULL,
    nama_bahan character varying(50) NOT NULL
);


ALTER TABLE farmakami.komposisi OWNER TO itxnrcnocpagke;

--
-- Name: konsumen; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.konsumen (
    id_konsumen character varying(10) NOT NULL,
    email character varying(50) NOT NULL,
    jenis_kelamin character varying(1) NOT NULL,
    tanggal_lahir date NOT NULL
);


ALTER TABLE farmakami.konsumen OWNER TO itxnrcnocpagke;

--
-- Name: kurir; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.kurir (
    id_kurir character varying(10) NOT NULL,
    email character varying(50) NOT NULL,
    nama_perusahaan character varying(50) NOT NULL
);


ALTER TABLE farmakami.kurir OWNER TO itxnrcnocpagke;

--
-- Name: list_produk_dibeli; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.list_produk_dibeli (
    jumlah integer NOT NULL,
    id_apotek character varying(10) NOT NULL,
    id_produk character varying(10) NOT NULL,
    id_transaksi_pembelian character varying(10) NOT NULL
);


ALTER TABLE farmakami.list_produk_dibeli OWNER TO itxnrcnocpagke;

--
-- Name: merk_dagang; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.merk_dagang (
    id_merk character varying(10) NOT NULL,
    nama_merk character varying(50) NOT NULL,
    perusahaan character varying(50) NOT NULL
);


ALTER TABLE farmakami.merk_dagang OWNER TO itxnrcnocpagke;

--
-- Name: merk_dagang_alat_medis; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.merk_dagang_alat_medis (
    id_alat_medis character varying(10) NOT NULL,
    id_merk character varying(10) NOT NULL,
    fitur text NOT NULL,
    dimensi character varying(10) NOT NULL,
    cara_penggunaan text,
    model character varying(20)
);


ALTER TABLE farmakami.merk_dagang_alat_medis OWNER TO itxnrcnocpagke;

--
-- Name: merk_obat; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.merk_obat (
    id_merk_obat character varying(10) NOT NULL,
    golongan character varying(25) NOT NULL,
    nama_dagang character varying(50) NOT NULL,
    perusahaan character varying(50) NOT NULL
);


ALTER TABLE farmakami.merk_obat OWNER TO itxnrcnocpagke;

--
-- Name: obat; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.obat (
    id_obat character varying(10) NOT NULL,
    id_produk character varying(10) NOT NULL,
    id_merk_obat character varying(10) NOT NULL,
    netto character varying(10) NOT NULL,
    dosis text NOT NULL,
    aturan_pakai text,
    kontraindikasi text,
    bentuk_kesediaan text NOT NULL
);


ALTER TABLE farmakami.obat OWNER TO itxnrcnocpagke;

--
-- Name: pengantaran_farmasi; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.pengantaran_farmasi (
    id_pengantaran character varying(10) NOT NULL,
    id_kurir character varying(10) NOT NULL,
    id_transaksi_pembelian character varying(10),
    waktu timestamp without time zone NOT NULL,
    status_pengantaran character varying(20) NOT NULL,
    biaya_kirim integer NOT NULL,
    total_biaya integer NOT NULL
);


ALTER TABLE farmakami.pengantaran_farmasi OWNER TO itxnrcnocpagke;

--
-- Name: pengguna; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.pengguna (
    email character varying(50) NOT NULL,
    telepon character varying(20),
    password character varying(128) NOT NULL,
    nama_lengkap character varying(50) NOT NULL
);


ALTER TABLE farmakami.pengguna OWNER TO itxnrcnocpagke;

--
-- Name: produk; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.produk (
    id_produk character varying(10) NOT NULL
);


ALTER TABLE farmakami.produk OWNER TO itxnrcnocpagke;

--
-- Name: produk_apotek; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.produk_apotek (
    harga_jual integer NOT NULL,
    stok integer NOT NULL,
    satuan_penjualan character varying(5) NOT NULL,
    id_produk character varying(10) NOT NULL,
    id_apotek character varying(10) NOT NULL
);


ALTER TABLE farmakami.produk_apotek OWNER TO itxnrcnocpagke;

--
-- Name: transaksi_dengan_resep; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.transaksi_dengan_resep (
    no_resep character varying(20) NOT NULL,
    nama_dokter character varying(50) NOT NULL,
    nama_pasien character varying(50) NOT NULL,
    isi_resep text NOT NULL,
    unggahan_resep text,
    tanggal_resep date NOT NULL,
    status_validasi character varying(20) NOT NULL,
    no_telepon character varying(20),
    id_transaksi_pembelian character varying(10) NOT NULL,
    email_apoteker character varying(50)
);


ALTER TABLE farmakami.transaksi_dengan_resep OWNER TO itxnrcnocpagke;

--
-- Name: transaksi_online; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.transaksi_online (
    no_urut_pembelian character varying(20) NOT NULL,
    id_transaksi_pembelian character varying(10)
);


ALTER TABLE farmakami.transaksi_online OWNER TO itxnrcnocpagke;

--
-- Name: transaksi_pembelian; Type: TABLE; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TABLE farmakami.transaksi_pembelian (
    id_transaksi_pembelian character varying(10) NOT NULL,
    waktu_pembelian timestamp without time zone NOT NULL,
    total_pembayaran integer DEFAULT 0 NOT NULL,
    id_konsumen character varying(10) NOT NULL
);


ALTER TABLE farmakami.transaksi_pembelian OWNER TO itxnrcnocpagke;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO itxnrcnocpagke;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: itxnrcnocpagke
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO itxnrcnocpagke;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itxnrcnocpagke
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO itxnrcnocpagke;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: itxnrcnocpagke
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO itxnrcnocpagke;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itxnrcnocpagke
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO itxnrcnocpagke;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: itxnrcnocpagke
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO itxnrcnocpagke;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itxnrcnocpagke
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO itxnrcnocpagke;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO itxnrcnocpagke;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: itxnrcnocpagke
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO itxnrcnocpagke;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itxnrcnocpagke
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: itxnrcnocpagke
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO itxnrcnocpagke;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itxnrcnocpagke
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO itxnrcnocpagke;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: itxnrcnocpagke
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO itxnrcnocpagke;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itxnrcnocpagke
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO itxnrcnocpagke;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: itxnrcnocpagke
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO itxnrcnocpagke;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itxnrcnocpagke
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO itxnrcnocpagke;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: itxnrcnocpagke
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO itxnrcnocpagke;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itxnrcnocpagke
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO itxnrcnocpagke;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: itxnrcnocpagke
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO itxnrcnocpagke;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itxnrcnocpagke
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: itxnrcnocpagke
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO itxnrcnocpagke;

--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Data for Name: admin_apotek; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.admin_apotek (email, id_apotek) FROM stdin;
dkiplingo@mac.com	1
ggreenalfg@bloglovin.com	2
emiddlebrooki@unc.edu	3
nyvenk@e-recht24.de	4
bleurenm@usnews.com	5
test@gmail.com	2
dilan@gmail.com	4
dilan	2
test@mail.com	3
ayam	4
\.


--
-- Data for Name: alamat_konsumen; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.alamat_konsumen (id_konsumen, alamat, status) FROM stdin;
5	1 Killdeer Court	Valid
4	16291 Ilene Road	Valid
1	163 Onsgard Circle	Valid
4	886 Village Green Place	Valid
4	68603 Main Drive	Valid
3	867 Scoville Circle	Valid
5	1544 Donald Alley	Valid
3	79772 Rieder Plaza	Valid
2	25 Thierer Plaza	Valid
5	6492 Miller Drive	Valid
\.


--
-- Data for Name: alat_medis; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.alat_medis (id_alat_medis, nama_alat_medis, deskripsi_alat, jenis_penggunaan, id_produk) FROM stdin;
1	Tortor Sollicitudin	Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.	multiple use	8
2	Turpis Nec	Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.	single use	1
3	Sed Vel	Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.	multiple use	13
4	Odio elementum	Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.	single use	12
5	Etiam	Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.	multiple use	7
\.


--
-- Data for Name: apotek; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.apotek (id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon_apotek) FROM stdin;
1	mlearmonth0@fc2.com	28540316593738563734	Douglas, Shanahan and Strosin	Ritchie-Littel	1810 Kensington Park	206-922-7570
2	yteliga1@nsw.gov.au	19318279886423236951	Rosenbaum Inc	Vandervort, Rippin and Cronin	85063 Dunning Plaza	586-962-6780
3	dhopkynson2@slate.com	54766148677582306869	Heller Inc	Kuvalis Inc	8 Reinke Way	112-923-3282
4	cgrief3@amazon.com	95284826948772965753	Waelchi-Will	Crona Inc	91 New Castle Avenue	297-217-8079
5	sethridge4@webs.com	59086351201033585854	Toy-Pollich	Prohaska, Ledner and O'Keefe	00201 Summerview Way	990-810-2740
\.


--
-- Data for Name: apoteker; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.apoteker (email) FROM stdin;
dkiplingo@mac.com
ggreenalfg@bloglovin.com
emiddlebrooki@unc.edu
nyvenk@e-recht24.de
bleurenm@usnews.com
test@gmail.com
dilan@gmail.com
dilan
test@mail.com
ayam
kheruzy13@gmail.com
\.


--
-- Data for Name: balai_apotek; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.balai_apotek (id_balai, id_apotek) FROM stdin;
1	2
2	3
4	5
5	4
3	1
\.


--
-- Data for Name: balai_pengobatan; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.balai_pengobatan (id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai) FROM stdin;
1	942 Columbus Terrace	Edgewire	purus	713-866-3508
2	60 Mallory Hill	Jaxworks	faucibus orci	478-215-7741
3	573 North Lane	Cogilith	porttitor pede	665-464-3147
4	384 Express Alley	Feedspan	porttitor	\N
5	9521 Del Sol Drive	Tagpad	ultrices enim	189-682-3652
\.


--
-- Data for Name: cs; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.cs (no_ktp, email, no_sia) FROM stdin;
45284760676063378218	nyvenk@e-recht24.de	19513981265734439029
29180402603902581655	dkiplingo@mac.com	32546816144913936636
87189916392057912393	emiddlebrooki@unc.edu	80817492488976916565
69577228654387535827	ggreenalfg@bloglovin.com	90957540353858933542
26820093517984468072	bleurenm@usnews.com	23139683927388016648
\.


--
-- Data for Name: kandungan; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.kandungan (id_bahan, id_obat, takaran, satuan_takar) FROM stdin;
1	2	120	mg
2	5	5	mg
3	6	15	ml
4	11	200	ml
5	3	15	ml
1	7	5	ml
2	11	15	ml
3	8	120	ml
4	11	60	mg
5	14	120	ml
1	6	60	mg
2	8	5	ml
3	11	60	ml
4	14	120	mg
5	6	200	mg
1	7	60	mg
2	3	120	mg
3	11	15	mg
4	15	200	ml
5	9	120	mg
\.


--
-- Data for Name: kategori_merk_obat; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.kategori_merk_obat (id_kategori, id_merk_obat) FROM stdin;
2	1
2	5
7	3
4	3
2	4
7	2
1	4
7	5
7	4
5	3
\.


--
-- Data for Name: kategori_obat; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.kategori_obat (id_kategori, nama_kategori, deskripsi_kategori) FROM stdin;
1	vitamin	Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.
2	flu	Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.
3	demam	\N
4	alergi	Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.
5	kulit	Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.
6	pusing	Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.
7	antibiotik	Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.
8	antiseptik	Proin eu mi.
9	batuk	In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.
10	batuk berdahak	Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.
\.


--
-- Data for Name: komposisi; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.komposisi (id_bahan, nama_bahan) FROM stdin;
1	Octinoxate and Titanium Dioxide
2	Bumetanide
3	KETOROLAC TROMETHAMINE
4	erythromycin
5	Gum, Sweet Liquidambar styraciflua
\.


--
-- Data for Name: konsumen; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.konsumen (id_konsumen, email, jenis_kelamin, tanggal_lahir) FROM stdin;
1	wslobom0@ucsd.edu	M	2010-08-01
2	acheetam2@hatena.ne.jp	M	1991-06-28
3	cgurery4@buzzfeed.com	F	2016-11-01
4	vfellos7@usda.gov	F	2014-06-23
5	iplaskett9@last.fm	F	1999-01-05
\.


--
-- Data for Name: kurir; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.kurir (id_kurir, email, nama_perusahaan) FROM stdin;
5	ncutcliffe5@networkadvertising.org	Janyx
1	aperroni6@shareasale.com	Livetube
2	vfellos7@usda.gov	Devpulse
4	pbertomeu8@amazon.co.jp	Skivee
3	iplaskett9@last.fm	Voomm
\.


--
-- Data for Name: list_produk_dibeli; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.list_produk_dibeli (jumlah, id_apotek, id_produk, id_transaksi_pembelian) FROM stdin;
2	5	3	6
3	4	14	5
4	5	12	9
5	3	14	2
6	3	9	8
8	4	1	15
7	3	7	9
9	2	17	1
10	4	19	11
1	1	5	3
\.


--
-- Data for Name: merk_dagang; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.merk_dagang (id_merk, nama_merk, perusahaan) FROM stdin;
1	Precision Dose Inc.	Raynor, Wehner and Sipes
2	Aloe Care International, LLC	Yundt, Green and Ziemann
3	Natures Way Holding Co.	Glover, Altenwerth and Goyette
4	Aurolife Pharma, LLC	DuBuque Group
5	BioActive Nutritional, Inc.	Mills-Bruen
6	DZA Brands LLC	Hayes, Orn and Mante
7	Heritage Pharmaceuticals Inc.	Bernier-Rolfson
8	Blenheim Pharmacal, Inc.	Zboncak Inc
9	La Prairie, Inc.	Sipes, Gislason and McClure
10	Bryant Ranch Prepack	Carter, Dare and Renner
\.


--
-- Data for Name: merk_dagang_alat_medis; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.merk_dagang_alat_medis (id_alat_medis, id_merk, fitur, dimensi, cara_penggunaan, model) FROM stdin;
5	2	Tensimeter	60X88X75	Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.	\N
1	8	Stetoskop	13X57X22	\N	Sierra 3500
5	4	Infus Set	17X11X37	Pellentesque viverra pede ac diam.	Vision
4	6	Kursi Roda	99X86X57	Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae, Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.	Tercel
3	3	Kruk	63X20X25	Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.	\N
3	8	Tongkat Bantu Jalan	05X37X44	Proin risus. Praesent lectus.	MR2
1	8	Hospital Bed	75X53X57	Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.	Cabriolet
2	1	Bedside Cabinet	57X56X26	Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.	Highlander
3	4	Incubator	20X26X99	Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae, Duis faucibus accumsan odio. Curabitur convallis.	Lanos
1	7	Infant Warmer	69X01X10	Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.	LeMans
\.


--
-- Data for Name: merk_obat; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.merk_obat (id_merk_obat, golongan, nama_dagang, perusahaan) FROM stdin;
1	13	thrombin, topical (bovine)	Pfizer Laboratories Div Pfizer Inc
2	4	Ibuprofen	Lil' Drug Store Products, Inc
3	20	Losartan Potassium and Hydrochlorothiazide	Watson Laboratories, Inc.
4	11	Avobenzone, Homosalate, Octinoxate	Avon Products, Inc.
5	18	OXYGEN	Saginaw Weldilng Supply Company
\.


--
-- Data for Name: obat; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.obat (id_obat, id_produk, id_merk_obat, netto, dosis, aturan_pakai, kontraindikasi, bentuk_kesediaan) FROM stdin;
1	11	1	250	1/2 tablet	Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.	Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.	In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.
2	15	3	150	1/2 tablet	\N	Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.	Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.
3	4	3	350	1 sendok makan	Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.	\N	Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.
4	18	3	350	1 tablet	Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.	Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.	Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.
5	15	2	400	1 sendok makan	Aenean auctor gravida sem.	Nulla facilisi. Cras non velit nec nisi vulputate nonummy.	In hac habitasse platea dictumst.
6	17	4	450	1 tablet	Fusce consequat. Nulla nisl. Nunc nisl.	\N	Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.
7	10	3	450	1/2 tablet	Morbi ut odio.	\N	Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.
8	17	1	150	1 sendok makan	Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.	\N	Nullam molestie nibh in lectus.
9	17	4	400	1/2 tablet	\N	\N	Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.
10	15	2	400	1 sendok makan	\N	Aenean fermentum. Donec ut mauris eget massa tempor convallis.	Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst.
11	5	4	250	1 tablet	\N	Etiam vel augue. Vestibulum rutrum rutrum neque.	Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.
12	20	3	150	1 sendok makan	\N	Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.	In hac habitasse platea dictumst.
13	17	1	150	1 tablet	Phasellus id sapien in sapien iaculis congue.	Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.	Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.
14	13	5	200	1 sendok makan	\N	Cras pellentesque volutpat dui.	Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.
15	17	5	150	1 tablet	Donec dapibus.	\N	Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.
\.


--
-- Data for Name: pengantaran_farmasi; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.pengantaran_farmasi (id_pengantaran, id_kurir, id_transaksi_pembelian, waktu, status_pengantaran, biaya_kirim, total_biaya) FROM stdin;
3	2	6	2020-01-29 20:02:00	Menunggu	836	11168
6	2	2	2020-01-29 18:19:00	Menunggu	824	6425
9	5	15	2020-01-29 06:19:00	Menunggu	266	22090
10	4	4	2020-01-29 18:28:00	Menunggu	576	576
2	3	3	2020-01-29 13:30:00	Menunggu	66	5997
5	3	5	2020-01-29 12:44:00	Menunggu	479	7277
7	3	3	2020-01-29 08:16:00	Menunggu	729	6660
1	3	15	2020-01-29 23:44:00	Menunggu	388	22212
11	1	1	2020-04-23 18:19:00	Menunggu	500	50477
8	1	1	2020-04-23 18:19:00	Menunggu	500	50477
4	1	10	2020-04-23 18:19:00	Menunggu	888	888
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.pengguna (email, telepon, password, nama_lengkap) FROM stdin;
jperkis1@google.es	804-126-0493	c9LkjTEme	Jourdan Perkis
acheetam2@hatena.ne.jp	981-454-0273	3cyMQCoYPk9	Ado Cheetam
ibodechon3@cornell.edu	846-904-3457	1N3oEp	Ingunna Bodechon
cgurery4@buzzfeed.com	337-103-3565	IuY5PfFiyhn	Carce Gurery
ncutcliffe5@networkadvertising.org	638-297-0916	IqZdOB6Kj6	Nigel Cutcliffe
aperroni6@shareasale.com	389-351-1281	FDCYNZVj	Alexio Perroni
vfellos7@usda.gov	481-856-0245	Y2Z1xTd173oq	Vina Fellos
pbertomeu8@amazon.co.jp	680-541-9309	Rtc5OYjD	Palm Bertomeu
iplaskett9@last.fm	440-312-2109	rXAgZeVW	Ivar Plaskett
brulfa@fastcompany.com	901-359-7491	GuwkjO	Bjorn Rulf
jtrowellb@1688.com	810-934-2282	8IR5Vcfvn	Jakie Trowell
rchelsomc@mit.edu	496-309-6623	E44OA7ufMK	Reeta Chelsom
amcclarend@mlb.com	273-918-2414	EdChPkOJD3	Agnola McClaren
dscorthornee@biglobe.ne.jp	655-779-5878	3hBuLkv1pzG	Dallon Scorthorne
amaciloryf@blog.com	615-538-5706	n3z2AeSRc	Alix MacIlory
ggreenalfg@bloglovin.com	306-149-9594	QOEFaYbxV1kW	Grantley Greenalf
bcantrellh@imgur.com	351-261-6163	xV1LgwBhsZnJ	Brandais Cantrell
emiddlebrooki@unc.edu	782-139-7479	DDK3uI1T0yxf	Elwin Middlebrook
jmckernonj@businesswire.com	862-345-9078	unGHaq0bv21	Juan McKernon
nyvenk@e-recht24.de	964-424-6377	iAXJ70pjbG	Nadine Yven
dtredgerl@flickr.com	201-964-8623	50nRMmRpu	Diane Tredger
bleurenm@usnews.com	338-703-1191	HoC8CfRq	Burnaby Leuren
abutfieldn@eepurl.com	918-615-4003	3Wkr1jMaPg4	Andrea Butfield
dkiplingo@mac.com	553-350-4512	vCWJdpK5	Danella Kipling
wslobom0@ucsd.edu	348-718-7059	oBthNFhUnx	Muhammad Erlangga
test@gmail.com	12345	test	testtest
dilan@gmail.com	081298789342	dilan	dilan
dilan	0721267876	dilan	dilan
test@mail.com	12345	123	test123
ayam	a	a	a
kheruzy13@gmail.com	5678767897678	fkheruzy123	fwawawa
\.


--
-- Data for Name: produk; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.produk (id_produk) FROM stdin;
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
\.


--
-- Data for Name: produk_apotek; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.produk_apotek (harga_jual, stok, satuan_penjualan, id_produk, id_apotek) FROM stdin;
2266	175	dus	14	4
1801	17	lmbar	5	4
5166	104	pcs	3	5
2392	14	pcs	8	3
4905	120	strip	1	5
1581	109	strip	9	3
5553	55	pcs	17	2
5893	43	strip	7	3
5931	11	botol	5	1
1867	95	pcs	14	3
1131	165	strip	15	5
1783	49	dus	8	1
1704	127	lmbar	12	2
2728	60	pcs	1	4
1699	55	dus	12	5
5662	5	pcs	12	4
1924	31	dus	3	3
3775	132	strip	5	5
2657	144	pcs	18	5
4778	181	lmbar	19	4
\.


--
-- Data for Name: transaksi_dengan_resep; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.transaksi_dengan_resep (no_resep, nama_dokter, nama_pasien, isi_resep, unggahan_resep, tanggal_resep, status_validasi, no_telepon, id_transaksi_pembelian, email_apoteker) FROM stdin;
1	Tessa Boerder	Francesco Rawls	Hydrocortisone	\N	2019-09-28	Processing	749-290-0667	2	dkiplingo@mac.com
3	Lee Capper	Layney Davall	One Seed Juniper	\N	2019-08-20	Processing	975-481-5360	2	ggreenalfg@bloglovin.com
5	Lefty Purser	Simone Renney	TITANIUM DIOXIDE, OCTINOXATE	Ultrices.txt	2019-04-26	Processing	639-734-8265	4	nyvenk@e-recht24.de
2	Nobie Feore	Fredek Haet	leuprolide acetate and norethindrone acetate	ScelerisqueMauris.txt	2019-04-26	Wait	399-687-2737	1	\N
4	Nelli Eberst	Stacee O'Shesnan	Bisoprolol fumarate	Orci.txt	2019-05-16	Wait	897-324-1635	5	\N
\.


--
-- Data for Name: transaksi_online; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.transaksi_online (no_urut_pembelian, id_transaksi_pembelian) FROM stdin;
3	2
2	3
1	5
5	4
4	1
\.


--
-- Data for Name: transaksi_pembelian; Type: TABLE DATA; Schema: farmakami; Owner: itxnrcnocpagke
--

COPY farmakami.transaksi_pembelian (id_transaksi_pembelian, waktu_pembelian, total_pembayaran, id_konsumen) FROM stdin;
14	2020-01-29 13:23:00	0	5
13	2020-01-29 10:22:00	0	1
4	2020-01-29 06:19:00	0	2
12	2020-01-29 18:19:00	0	3
7	2020-01-29 18:28:00	0	3
10	2020-01-29 17:20:00	0	1
6	2020-01-29 20:50:00	10332	5
5	2020-01-29 21:49:00	6798	1
2	2020-01-29 12:44:00	5601	3
8	2020-01-29 02:10:00	9486	4
15	2020-01-29 20:02:00	21824	4
9	2020-01-29 12:18:00	48047	2
11	2020-01-29 08:16:00	47780	1
3	2020-01-29 13:30:00	5931	5
16	2020-05-05 06:30:23.194316	0	2
17	2020-05-05 09:30:27.312293	0	1
1	2020-01-29 23:44:00	49977	3
18	2020-05-05 09:45:24.018349	0	5
19	2020-05-05 10:02:40.948108	0	1
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2020-05-01 09:56:32.792731+00
2	auth	0001_initial	2020-05-01 09:56:36.103806+00
3	admin	0001_initial	2020-05-01 09:56:44.634432+00
4	admin	0002_logentry_remove_auto_add	2020-05-01 09:56:47.204461+00
5	admin	0003_logentry_add_action_flag_choices	2020-05-01 09:56:48.547316+00
6	contenttypes	0002_remove_content_type_name	2020-05-01 09:56:50.453525+00
7	auth	0002_alter_permission_name_max_length	2020-05-01 09:56:51.957917+00
8	auth	0003_alter_user_email_max_length	2020-05-01 09:56:53.569813+00
9	auth	0004_alter_user_username_opts	2020-05-01 09:56:54.668333+00
10	auth	0005_alter_user_last_login_null	2020-05-01 09:56:56.074022+00
11	auth	0006_require_contenttypes_0002	2020-05-01 09:56:57.37787+00
12	auth	0007_alter_validators_add_error_messages	2020-05-01 09:56:58.53381+00
13	auth	0008_alter_user_username_max_length	2020-05-01 09:57:00.188948+00
14	auth	0009_alter_user_last_name_max_length	2020-05-01 09:57:01.796954+00
15	auth	0010_alter_group_name_max_length	2020-05-01 09:57:03.24671+00
16	auth	0011_update_proxy_permissions	2020-05-01 09:57:04.704203+00
19	sessions	0001_initial	2020-05-04 16:56:57.305378+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: itxnrcnocpagke
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
1mn37yqky4hp7phwpuqjt2z2xga25yv6	Y2FkODdiYjNjZDZlM2Y2OWM4ZjM2OWRkMWFiNDQxNGEwNGM5NDA1Mzp7ImVtYWlsIjoiZGlsYW4iLCJ0ZWxlcG9uIjoiMDcyMTI2Nzg3NiIsInBhc3N3b3JkIjoiZGlsYW4iLCJuYW1hX2xlbmdrYXAiOiJkaWxhbiIsInJvbGUiOiJBZG1pbiJ9	2020-05-19 07:33:04.699652+00
71fkngmmrdfpmjuz39w3zt9x8vvxxuhy	ZjJlNjIwNzFiN2VmMGU3NzFjN2ExYWU5YTNjNmM3ZjE2YmEwODNjNDp7ImVtYWlsIjoidGVzdEBnbWFpbC5jb20iLCJ0ZWxlcG9uIjoiMTIzNDUiLCJwYXNzd29yZCI6InRlc3QiLCJuYW1hX2xlbmdrYXAiOiJ0ZXN0dGVzdCIsInJvbGUiOiJBZG1pbiJ9	2020-05-19 09:28:46.945543+00
js0gvncz62ybbxlodu4phqhsa91vjxub	OTdmZWI3MGQ3YzhmMTk5ZDBiODc2Zjk3MTE3OTc0YzdiMjI2Njg1MTp7ImVtYWlsIjoid3Nsb2JvbTBAdWNzZC5lZHUiLCJ0ZWxlcG9uIjoiMzQ4LTcxOC03MDU5IiwicGFzc3dvcmQiOiJvQnRoTkZoVW54IiwibmFtYV9sZW5na2FwIjoiTXVoYW1tYWQgRXJsYW5nZ2EiLCJyb2xlIjoiS29uc3VtZW4ifQ==	2020-05-19 09:47:20.881815+00
rk0zdxa1udzrh4o5uawk7yhwv6hed7qr	YTc2YmQwYjAzZjI0MDNmNmU1Y2ZjNGViN2RmNTAyNmYzYWUxOGY1Nzp7ImVtYWlsIjoidGVzdEBtYWlsLmNvbSIsInRlbGVwb24iOiIxMjM0NSIsInBhc3N3b3JkIjoiMTIzIiwibmFtYV9sZW5na2FwIjoidGVzdDEyMyIsInJvbGUiOiJBZG1pbiJ9	2020-05-19 10:20:32.937766+00
9od7z3kfb6dmgnxk31hh8gyl4ehmshfu	Y2FkODdiYjNjZDZlM2Y2OWM4ZjM2OWRkMWFiNDQxNGEwNGM5NDA1Mzp7ImVtYWlsIjoiZGlsYW4iLCJ0ZWxlcG9uIjoiMDcyMTI2Nzg3NiIsInBhc3N3b3JkIjoiZGlsYW4iLCJuYW1hX2xlbmdrYXAiOiJkaWxhbiIsInJvbGUiOiJBZG1pbiJ9	2020-05-19 12:58:02.113179+00
gk037g76ga2de26q8uegc86suw9n7vjv	OTdmZWI3MGQ3YzhmMTk5ZDBiODc2Zjk3MTE3OTc0YzdiMjI2Njg1MTp7ImVtYWlsIjoid3Nsb2JvbTBAdWNzZC5lZHUiLCJ0ZWxlcG9uIjoiMzQ4LTcxOC03MDU5IiwicGFzc3dvcmQiOiJvQnRoTkZoVW54IiwibmFtYV9sZW5na2FwIjoiTXVoYW1tYWQgRXJsYW5nZ2EiLCJyb2xlIjoiS29uc3VtZW4ifQ==	2020-05-19 13:45:53.880932+00
53ntcc9c7wsbwya1s3ucq69c115y6gut	Y2FkODdiYjNjZDZlM2Y2OWM4ZjM2OWRkMWFiNDQxNGEwNGM5NDA1Mzp7ImVtYWlsIjoiZGlsYW4iLCJ0ZWxlcG9uIjoiMDcyMTI2Nzg3NiIsInBhc3N3b3JkIjoiZGlsYW4iLCJuYW1hX2xlbmdrYXAiOiJkaWxhbiIsInJvbGUiOiJBZG1pbiJ9	2020-05-19 14:35:40.273597+00
nnhgdhetsigqp3yovbxo3c78yfijtdc3	M2NjNmMyMzVlMzI4M2JiZmU3OWJhMTk2MTI5YTE3MjUxZGJjNjBiYzp7ImVtYWlsIjoia2hlcnV6eTEzQGdtYWlsLmNvbSIsInRlbGVwb24iOiI1Njc4NzY3ODk3Njc4IiwicGFzc3dvcmQiOiJma2hlcnV6eTEyMyIsIm5hbWFfbGVuZ2thcCI6ImZ3YXdhd2EiLCJyb2xlIjpudWxsfQ==	2020-05-19 14:45:03.416467+00
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itxnrcnocpagke
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itxnrcnocpagke
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itxnrcnocpagke
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 24, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itxnrcnocpagke
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itxnrcnocpagke
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, false);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itxnrcnocpagke
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itxnrcnocpagke
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itxnrcnocpagke
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 6, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itxnrcnocpagke
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 19, true);


--
-- Name: admin_apotek admin_apotek_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.admin_apotek
    ADD CONSTRAINT admin_apotek_pkey PRIMARY KEY (email);


--
-- Name: alamat_konsumen alamat_konsumen_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.alamat_konsumen
    ADD CONSTRAINT alamat_konsumen_pkey PRIMARY KEY (id_konsumen, alamat, status);


--
-- Name: alat_medis alat_medis_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.alat_medis
    ADD CONSTRAINT alat_medis_pkey PRIMARY KEY (id_alat_medis);


--
-- Name: apotek apotek_alamat_apotek_key; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.apotek
    ADD CONSTRAINT apotek_alamat_apotek_key UNIQUE (alamat_apotek);


--
-- Name: apotek apotek_email_key; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.apotek
    ADD CONSTRAINT apotek_email_key UNIQUE (email);


--
-- Name: apotek apotek_no_sia_key; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.apotek
    ADD CONSTRAINT apotek_no_sia_key UNIQUE (no_sia);


--
-- Name: apotek apotek_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.apotek
    ADD CONSTRAINT apotek_pkey PRIMARY KEY (id_apotek);


--
-- Name: apoteker apoteker_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.apoteker
    ADD CONSTRAINT apoteker_pkey PRIMARY KEY (email);


--
-- Name: balai_apotek balai_apotek_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.balai_apotek
    ADD CONSTRAINT balai_apotek_pkey PRIMARY KEY (id_balai, id_apotek);


--
-- Name: balai_pengobatan balai_pengobatan_alamat_balai_key; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.balai_pengobatan
    ADD CONSTRAINT balai_pengobatan_alamat_balai_key UNIQUE (alamat_balai);


--
-- Name: balai_pengobatan balai_pengobatan_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.balai_pengobatan
    ADD CONSTRAINT balai_pengobatan_pkey PRIMARY KEY (id_balai);


--
-- Name: cs cs_no_sia_key; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.cs
    ADD CONSTRAINT cs_no_sia_key UNIQUE (no_sia);


--
-- Name: cs cs_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.cs
    ADD CONSTRAINT cs_pkey PRIMARY KEY (no_ktp);


--
-- Name: kandungan kandungan_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kandungan
    ADD CONSTRAINT kandungan_pkey PRIMARY KEY (id_bahan, id_obat, takaran);


--
-- Name: kategori_merk_obat kategori_merk_obat_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kategori_merk_obat
    ADD CONSTRAINT kategori_merk_obat_pkey PRIMARY KEY (id_kategori, id_merk_obat);


--
-- Name: kategori_obat kategori_obat_nama_kategori_key; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kategori_obat
    ADD CONSTRAINT kategori_obat_nama_kategori_key UNIQUE (nama_kategori);


--
-- Name: kategori_obat kategori_obat_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kategori_obat
    ADD CONSTRAINT kategori_obat_pkey PRIMARY KEY (id_kategori);


--
-- Name: komposisi komposisi_nama_bahan_key; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.komposisi
    ADD CONSTRAINT komposisi_nama_bahan_key UNIQUE (nama_bahan);


--
-- Name: komposisi komposisi_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.komposisi
    ADD CONSTRAINT komposisi_pkey PRIMARY KEY (id_bahan);


--
-- Name: konsumen konsumen_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.konsumen
    ADD CONSTRAINT konsumen_pkey PRIMARY KEY (id_konsumen);


--
-- Name: kurir kurir_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kurir
    ADD CONSTRAINT kurir_pkey PRIMARY KEY (id_kurir);


--
-- Name: list_produk_dibeli list_produk_dibeli_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.list_produk_dibeli
    ADD CONSTRAINT list_produk_dibeli_pkey PRIMARY KEY (id_apotek, id_produk, id_transaksi_pembelian);


--
-- Name: merk_dagang_alat_medis merk_dagang_alat_medis_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.merk_dagang_alat_medis
    ADD CONSTRAINT merk_dagang_alat_medis_pkey PRIMARY KEY (id_alat_medis, id_merk, fitur);


--
-- Name: merk_dagang merk_dagang_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.merk_dagang
    ADD CONSTRAINT merk_dagang_pkey PRIMARY KEY (id_merk);


--
-- Name: merk_obat merk_obat_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.merk_obat
    ADD CONSTRAINT merk_obat_pkey PRIMARY KEY (id_merk_obat);


--
-- Name: obat obat_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.obat
    ADD CONSTRAINT obat_pkey PRIMARY KEY (id_obat);


--
-- Name: pengantaran_farmasi pengantaran_farmasi_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.pengantaran_farmasi
    ADD CONSTRAINT pengantaran_farmasi_pkey PRIMARY KEY (id_pengantaran);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (email);


--
-- Name: produk_apotek produk_apotek_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.produk_apotek
    ADD CONSTRAINT produk_apotek_pkey PRIMARY KEY (id_produk, id_apotek);


--
-- Name: produk produk_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.produk
    ADD CONSTRAINT produk_pkey PRIMARY KEY (id_produk);


--
-- Name: transaksi_dengan_resep transaksi_dengan_resep_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.transaksi_dengan_resep
    ADD CONSTRAINT transaksi_dengan_resep_pkey PRIMARY KEY (no_resep);


--
-- Name: transaksi_online transaksi_online_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.transaksi_online
    ADD CONSTRAINT transaksi_online_pkey PRIMARY KEY (no_urut_pembelian);


--
-- Name: transaksi_pembelian transaksi_pembelian_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.transaksi_pembelian
    ADD CONSTRAINT transaksi_pembelian_pkey PRIMARY KEY (id_transaksi_pembelian);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: itxnrcnocpagke
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: transaksi_dengan_resep assign_apoteker_trigger; Type: TRIGGER; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TRIGGER assign_apoteker_trigger AFTER INSERT OR UPDATE ON farmakami.transaksi_dengan_resep FOR EACH ROW EXECUTE FUNCTION farmakami.assign_apoteker();


--
-- Name: pengantaran_farmasi pembuatan_pengantaran_farmasi; Type: TRIGGER; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TRIGGER pembuatan_pengantaran_farmasi AFTER INSERT ON farmakami.pengantaran_farmasi FOR EACH ROW EXECUTE FUNCTION farmakami.pembuatan_pengantaran_farmasi();


--
-- Name: list_produk_dibeli trigger_update_total_pembayaran; Type: TRIGGER; Schema: farmakami; Owner: itxnrcnocpagke
--

CREATE TRIGGER trigger_update_total_pembayaran AFTER INSERT OR DELETE OR UPDATE ON farmakami.list_produk_dibeli FOR EACH ROW EXECUTE FUNCTION farmakami.update_total_pembayaran();


--
-- Name: admin_apotek admin_apotek_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.admin_apotek
    ADD CONSTRAINT admin_apotek_email_fkey FOREIGN KEY (email) REFERENCES farmakami.apoteker(email);


--
-- Name: admin_apotek admin_apotek_id_apotek_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.admin_apotek
    ADD CONSTRAINT admin_apotek_id_apotek_fkey FOREIGN KEY (id_apotek) REFERENCES farmakami.apotek(id_apotek);


--
-- Name: alamat_konsumen alamat_konsumen_id_konsumen_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.alamat_konsumen
    ADD CONSTRAINT alamat_konsumen_id_konsumen_fkey FOREIGN KEY (id_konsumen) REFERENCES farmakami.konsumen(id_konsumen) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: alat_medis alat_medis_id_produk_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.alat_medis
    ADD CONSTRAINT alat_medis_id_produk_fkey FOREIGN KEY (id_produk) REFERENCES farmakami.produk(id_produk);


--
-- Name: apoteker apoteker_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.apoteker
    ADD CONSTRAINT apoteker_email_fkey FOREIGN KEY (email) REFERENCES farmakami.pengguna(email);


--
-- Name: balai_apotek balai_apotek_id_apotek_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.balai_apotek
    ADD CONSTRAINT balai_apotek_id_apotek_fkey FOREIGN KEY (id_apotek) REFERENCES farmakami.apotek(id_apotek);


--
-- Name: balai_apotek balai_apotek_id_balai_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.balai_apotek
    ADD CONSTRAINT balai_apotek_id_balai_fkey FOREIGN KEY (id_balai) REFERENCES farmakami.balai_pengobatan(id_balai);


--
-- Name: cs cs_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.cs
    ADD CONSTRAINT cs_email_fkey FOREIGN KEY (email) REFERENCES farmakami.apoteker(email);


--
-- Name: kandungan kandungan_id_bahan_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kandungan
    ADD CONSTRAINT kandungan_id_bahan_fkey FOREIGN KEY (id_bahan) REFERENCES farmakami.komposisi(id_bahan);


--
-- Name: kandungan kandungan_id_obat_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kandungan
    ADD CONSTRAINT kandungan_id_obat_fkey FOREIGN KEY (id_obat) REFERENCES farmakami.obat(id_obat);


--
-- Name: kategori_merk_obat kategori_merk_obat_id_kategori_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kategori_merk_obat
    ADD CONSTRAINT kategori_merk_obat_id_kategori_fkey FOREIGN KEY (id_kategori) REFERENCES farmakami.kategori_obat(id_kategori);


--
-- Name: kategori_merk_obat kategori_merk_obat_id_merk_obat_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kategori_merk_obat
    ADD CONSTRAINT kategori_merk_obat_id_merk_obat_fkey FOREIGN KEY (id_merk_obat) REFERENCES farmakami.merk_obat(id_merk_obat);


--
-- Name: konsumen konsumen_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.konsumen
    ADD CONSTRAINT konsumen_email_fkey FOREIGN KEY (email) REFERENCES farmakami.pengguna(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kurir kurir_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.kurir
    ADD CONSTRAINT kurir_email_fkey FOREIGN KEY (email) REFERENCES farmakami.pengguna(email);


--
-- Name: list_produk_dibeli list_produk_dibeli_id_produk_id_apotek_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.list_produk_dibeli
    ADD CONSTRAINT list_produk_dibeli_id_produk_id_apotek_fkey FOREIGN KEY (id_produk, id_apotek) REFERENCES farmakami.produk_apotek(id_produk, id_apotek);


--
-- Name: list_produk_dibeli list_produk_dibeli_id_transaksi_pembelian_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.list_produk_dibeli
    ADD CONSTRAINT list_produk_dibeli_id_transaksi_pembelian_fkey FOREIGN KEY (id_transaksi_pembelian) REFERENCES farmakami.transaksi_pembelian(id_transaksi_pembelian) ON DELETE CASCADE;


--
-- Name: merk_dagang_alat_medis merk_dagang_alat_medis_id_alat_medis_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.merk_dagang_alat_medis
    ADD CONSTRAINT merk_dagang_alat_medis_id_alat_medis_fkey FOREIGN KEY (id_alat_medis) REFERENCES farmakami.alat_medis(id_alat_medis);


--
-- Name: merk_dagang_alat_medis merk_dagang_alat_medis_id_merk_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.merk_dagang_alat_medis
    ADD CONSTRAINT merk_dagang_alat_medis_id_merk_fkey FOREIGN KEY (id_merk) REFERENCES farmakami.merk_dagang(id_merk);


--
-- Name: obat obat_id_merk_obat_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.obat
    ADD CONSTRAINT obat_id_merk_obat_fkey FOREIGN KEY (id_merk_obat) REFERENCES farmakami.merk_obat(id_merk_obat);


--
-- Name: obat obat_id_produk_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.obat
    ADD CONSTRAINT obat_id_produk_fkey FOREIGN KEY (id_produk) REFERENCES farmakami.produk(id_produk);


--
-- Name: pengantaran_farmasi pengantaran_farmasi_id_kurir_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.pengantaran_farmasi
    ADD CONSTRAINT pengantaran_farmasi_id_kurir_fkey FOREIGN KEY (id_kurir) REFERENCES farmakami.kurir(id_kurir);


--
-- Name: pengantaran_farmasi pengantaran_farmasi_id_transaksi_pembelian_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.pengantaran_farmasi
    ADD CONSTRAINT pengantaran_farmasi_id_transaksi_pembelian_fkey FOREIGN KEY (id_transaksi_pembelian) REFERENCES farmakami.transaksi_pembelian(id_transaksi_pembelian) ON DELETE CASCADE;


--
-- Name: produk_apotek produk_apotek_id_apotek_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.produk_apotek
    ADD CONSTRAINT produk_apotek_id_apotek_fkey FOREIGN KEY (id_apotek) REFERENCES farmakami.apotek(id_apotek);


--
-- Name: produk_apotek produk_apotek_id_produk_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.produk_apotek
    ADD CONSTRAINT produk_apotek_id_produk_fkey FOREIGN KEY (id_produk) REFERENCES farmakami.produk(id_produk);


--
-- Name: transaksi_dengan_resep transaksi_dengan_resep_email_apoteker_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.transaksi_dengan_resep
    ADD CONSTRAINT transaksi_dengan_resep_email_apoteker_fkey FOREIGN KEY (email_apoteker) REFERENCES farmakami.apoteker(email);


--
-- Name: transaksi_dengan_resep transaksi_dengan_resep_id_transaksi_pembelian_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.transaksi_dengan_resep
    ADD CONSTRAINT transaksi_dengan_resep_id_transaksi_pembelian_fkey FOREIGN KEY (id_transaksi_pembelian) REFERENCES farmakami.transaksi_pembelian(id_transaksi_pembelian) ON DELETE CASCADE;


--
-- Name: transaksi_online transaksi_online_id_transaksi_pembelian_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.transaksi_online
    ADD CONSTRAINT transaksi_online_id_transaksi_pembelian_fkey FOREIGN KEY (id_transaksi_pembelian) REFERENCES farmakami.transaksi_pembelian(id_transaksi_pembelian) ON DELETE CASCADE;


--
-- Name: transaksi_pembelian transaksi_pembelian_id_konsumen_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY farmakami.transaksi_pembelian
    ADD CONSTRAINT transaksi_pembelian_id_konsumen_fkey FOREIGN KEY (id_konsumen) REFERENCES farmakami.konsumen(id_konsumen);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: itxnrcnocpagke
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: itxnrcnocpagke
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO itxnrcnocpagke;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: LANGUAGE plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO itxnrcnocpagke;


--
-- PostgreSQL database dump complete
--

