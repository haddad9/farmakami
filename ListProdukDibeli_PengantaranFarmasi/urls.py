from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('produk_dibeli/create/', createProdukDibeli, name='createProdukDibeli'),
    path('produk_dibeli/update/<str:id_apotek>-<str:id_produk>-<str:id_transaksi_pembelian>/', updateProdukDibeli, name='updateProdukDibeli'),
    path('produk_dibeli/delete/<str:id_apotek>-<str:id_produk>-<str:id_transaksi_pembelian>/', deleteProdukDibeli, name='deleteProdukDibeli'),
    path('produk_dibeli/', readProdukDibeli, name='readProdukDibeli'),

    path('pengantaran_farmasi/create/', createPengantaranFarmasi, name='createPengantaranFarmasi'),
    path('pengantaran_farmasi/update/<str:id_pengantaran>/', updatePengantaranFarmasi, name='updatePengantaranFarmasi'),
    path('pengantaran_farmasi/delete/<str:id_pengantaran>/', deletePengantaranFarmasi, name='deletePengantaranFarmasi'),
    path('pengantaran_farmasi/', readPengantaranFarmasi, name='readPengantaranFarmasi'),
]
