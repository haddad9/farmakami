from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

# LIST PRODUK DIBELI

def readProdukDibeli(request):
    cursor = connection.cursor()
    # checkPrivilege(request.session)
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False
    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
        if (request.session['role'] == 'Konsumen'):
            roleKonsumen = True
        if (request.session['role'] == 'Kurir'):
            roleKurir = True
        if (request.session['role'] == 'CS'):
            roleCS = True
        email = str(request.session['email'])
    except:
        return reverse('login')

    cursor.execute("set search_path to farmakami")

    # if (roleKonsumen):
    #     # query = "select * from "
    #     # query += "list_transaksi_pembelian l, transaksi_pembelian t, konsumen k "
    #     # query += "where t.id_konsumen = k.id_konsumen "
    #     # query += "and k.email = " + email + " order by id_apotek::int"
    #     cursor.execute(query)
    # else:
    cursor.execute("select * from list_produk_dibeli order by id_apotek::int")
    hasil = namedtuplefetchall(cursor)

    argument = {
        'hasil' : hasil,
        'roleAdmin' : roleAdmin,
        'roleKonsumen' : roleKonsumen,
        'roleKurir' : roleKurir,
        'roleCS' : roleCS
    }
    return render(request, "readProdukDibeli.html", argument)

def createProdukDibeli(request):
    message = ""
    id_apotek = '1'
    id_produk = '1'
    id_transaksi_pembelian = '1'
    if request.method =='POST':
        try:
            input = request.POST
            cursor = connection.cursor()
            cursor.execute("set search_path to farmakami")
            cursor.execute("insert into list_produk_dibeli values ("+input['jumlah']+",'"+input['id_apotek']+"','"+input['id_produk']+"','"+input['id_transaksi_pembelian']+"')")
            return redirect('/produk_dibeli/')
        except:
            message = "ID Apotek " + input['id_apotek'] + " tidak memiliki ID Produk " + input['id_produk']
            id_apotek = input['id_apotek']
            id_produk = input['id_produk']
            id_transaksi_pembelian = input['id_transaksi_pembelian']
            pass

    
    form = CreateProdukDibeli(initial={
        'id_apotek' : id_apotek,
        'id_produk' : id_produk,
        'id_transaksi_pembelian' : id_transaksi_pembelian,
    }) 
    args = {
        'form': form,
        'message': message,
    }
    return render(request, 'createProdukDibeli.html', args)

def updateProdukDibeli(request, id_apotek, id_produk, id_transaksi_pembelian):
    message = ""
    if request.method =='POST':
        try:
            input = request.POST
            cursor = connection.cursor()
            cursor.execute("set search_path to farmakami")
            query = "update list_produk_dibeli"
            query += (" set id_transaksi_pembelian = '" + input['id_transaksi_pembelian'] + "' ")
            query += (", jumlah = '" + input['jumlah'] + "' ")
            query += ("where id_apotek = '" + id_apotek + "' ")
            query += ("and id_produk = '" + id_produk +"' ")
            cursor.execute(query)
            return redirect('/produk_dibeli/')
        except:
            message = "Terjadi kesalahan saat update produk dibeli"
            pass

    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    query = "select jumlah from "
    query += "list_produk_dibeli "
    query += ("where id_apotek = '" + id_apotek + "' ")
    query += ("and id_produk = '" + id_produk + "' ")
    query += ("and id_transaksi_pembelian = '" + id_transaksi_pembelian + "' ")
    cursor.execute(query)
    hasil = namedtuplefetchall(cursor)

    form = UpdateProdukDibeli(initial={
        'id_apotek' : id_apotek,
        'id_produk' : id_produk,
        'id_transaksi_pembelian' : id_transaksi_pembelian,
        'jumlah' : hasil[0].jumlah,
    }) 
    # form.fields['id_apotek'].widget = forms.HiddenInput()
    # form.fields['id_produk'].widget = forms.HiddenInput()
    form.fields['id_apotek'].widget.attrs['disabled'] = 'disabled'
    form.fields['id_produk'].widget.attrs['disabled'] = 'disabled'
    args = {
        'form': form,
        'id_apotek': id_apotek,
        'id_produk': id_produk,
        'id_transaksi_pembelian': id_transaksi_pembelian,
        'message' : message,
    }

    return render(request, "updateProdukDibeli.html", args)

def deleteProdukDibeli(request, id_apotek, id_produk, id_transaksi_pembelian):
    cursor = connection.cursor()
    # id_apotek = kwargs['id_apotek']
    # id_produk = kwargs['id_produk']
    # id_transaksi_pembelian = kwargs['id_transaksi_pembelian']
    print(id_apotek)
    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from list_produk_dibeli where id_transaksi_pembelian = '"+id_transaksi_pembelian+"' and id_apotek = '"+id_apotek+"' and id_produk ='"+id_produk+"'")
    return redirect('/produk_dibeli/')

# PENGANTARAN FARMASI

def readPengantaranFarmasi(request):
    cursor = connection.cursor()
    # checkPrivilege(request.session)
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False
    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
        if (request.session['role'] == 'Konsumen'):
            roleKonsumen = True
        if (request.session['role'] == 'Kurir'):
            roleKurir = True
        if (request.session['role'] == 'CS'):
            roleCS = True
        email = str(request.session['email'])
    except:
        return reverse('login')

    cursor.execute("set search_path to farmakami")
    if (roleKurir):
        query = "select p.* from "
        query += "pengantaran_farmasi p, kurir k "
        query += ("where k.email = '" + email +"' ")
        query += ("and k.id_kurir = p.id_kurir ")
        query += "order by p.id_pengantaran::int"
        cursor.execute(query)
    else:
        cursor.execute("select * from pengantaran_farmasi order by id_pengantaran::int")

    hasil = namedtuplefetchall(cursor)
    argument = {
        'hasil' : hasil,
        'roleAdmin' : roleAdmin,
        'roleKonsumen' : roleKonsumen,
        'roleKurir' : roleKurir,
        'roleCS' : roleCS
    }
    return render(request, "readPengantaranFarmasi.html", argument)

def max_index():
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_pengantaran from pengantaran_farmasi order by id_pengantaran::int")
    result = namedtuplefetchall(cursor)
    idx = []
    for a in result:
        result_string = str(a)
        idx_string = result_string[23:-2]
        idx.append(int(idx_string))
    return idx[-1]

def createPengantaranFarmasi(request):
    idx_next = max_index() + 1

    print(idx_next)
    if request.method =='POST':
        try:
            cursor = connection.cursor()
            cursor.execute("set search_path to farmakami")
            input = request.POST
            cursor.execute("insert into pengantaran_farmasi values ('"+str(idx_next)+"','"+input['id_kurir']+"','"+input['id_transaksi_pembelian']+"','"+input['waktu']+"', 'Processing',"+input['biaya_kirim']+",0)")
            return redirect('/pengantaran_farmasi/')
        except:
            message = "Terjadi kesalahan saat membuat item"
            pass

    form = CreatePengantaranFarmasi()
    args = {'form': form}
    return render(request, 'createPengantaranFarmasi.html', args)

def updatePengantaranFarmasi(request, id_pengantaran):
    message = ""
    if request.method =='POST':
        try:
            input = request.POST
            cursor = connection.cursor()
            cursor.execute("set search_path to farmakami")
            query = "update pengantaran_farmasi "
            query += ("set id_kurir = '" + input['id_kurir'] + "' ")
            query += (", id_transaksi_pembelian = '" + input['id_transaksi_pembelian'] + "' ")
            query += (", waktu = '" + input['waktu'] + "' ")
            query += (", biaya_kirim = '" + input['biaya_kirim'] + "' ")
            query += (", status_pengantaran = '" + input['status_pengantaran'] + "' ")
            query += ("where id_pengantaran = '" + id_pengantaran + "' ")
            cursor.execute(query)
            return redirect('/pengantaran_farmasi/')
        except:
            message = "Maaf, barang tersebut sedang diantar"
            pass
    
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    query = "select * from "
    query += "pengantaran_farmasi "
    query += ("where id_pengantaran = '" + id_pengantaran + "' ")
    cursor.execute(query)
    hasil = namedtuplefetchall(cursor)
    form = UpdatePengantaranFarmasi(initial={
        'id_pengantaran': id_pengantaran,
        'id_kurir': hasil[0].id_kurir,
        'id_transaksi_pembelian': hasil[0].id_transaksi_pembelian,
        'waktu': hasil[0].waktu,
        'status_pengantaran': hasil[0].status_pengantaran,
        'biaya_kirim': hasil[0].biaya_kirim,
    })
    form.fields['id_pengantaran'].widget.attrs['disabled'] = 'disabled'
    args = {
        'form': form,
        'id_pengantaran': id_pengantaran,
        'message' : message,
    }
    return render(request, "updatePengantaranFarmasi.html", args)

def deletePengantaranFarmasi(request, id_pengantaran):
    id_pengantaran = str(id_pengantaran)
    print(id_pengantaran)
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from pengantaran_farmasi where id_pengantaran = '"+id_pengantaran+"'")
    return redirect('/pengantaran_farmasi/')