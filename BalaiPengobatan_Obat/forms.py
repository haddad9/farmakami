from django import forms
from django.db import connection
from collections import namedtuple

cursor = connection.cursor()
cursor.execute("select id_apotek from farmakami.apotek")
select = cursor.fetchall()
id_apotek_berasosiasi = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

class createBalaiPengobatanForms(forms.Form):
    alamat_balai = forms.CharField(label='Alamat Balai', max_length=100)
    nama_balai = forms.CharField(label='Nama Balai', max_length=20)
    jenis_balai = forms.CharField(label='Jenis Balai', max_length=20)
    telepon_balai = forms.CharField(label='Telepon Balai', required= False, max_length=20)
    id_apotek = forms.CharField(max_length=10, widget=forms.Select(choices=id_apotek_berasosiasi, attrs={'class': 'form-control','placeholder' : 'Apotek Bersosiasi','required': True,}))

class createObatForm(forms.Form):
    # id_merk_obat = forms.CharField(label='ID Merk Obat', max_length=10, widget=forms.Select(choices=ID_MERK_OBAT))
    netto = forms.CharField(label='Netto', max_length=10)
    dosis = forms.CharField(label='Dosis', max_length=100)
    aturan_pakai = forms.CharField(label='Aturan Pakai', required= False, max_length=100)
    kontraindikasi = forms.CharField(label='Kontraindikasi', required= False, max_length=100)
    bentuk_kesediaan = forms.CharField(label='Bentuk Kesediaan', max_length=100)

class updateBalaiPengobatanForms(forms.Form):
    id_balai = forms.CharField(label='ID Balai', max_length=10, disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    alamat_balai = forms.CharField(label='Alamat Balai', max_length=100)
    nama_balai = forms.CharField(label='Nama Balai', max_length=20)
    jenis_balai = forms.CharField(label='Jenis Balai', max_length=20)
    telepon_balai = forms.CharField(label='Telepon Balai', required= False, max_length=20)
    id_apotek = forms.CharField(max_length=10 ,widget=forms.Select(choices=id_apotek_berasosiasi, attrs={'class': 'form-control','required': True,}))

class updateObatForm(forms.Form):
    id_obat = forms.CharField(label='ID Obat', max_length=10, disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    id_produk = forms.CharField(label='ID Produk', max_length=10, disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    netto = forms.CharField(label='Netto', max_length=10)
    dosis = forms.CharField(label='Dosis', max_length=100)
    aturan_pakai = forms.CharField(label='Aturan Pakai', required= False, max_length=100)
    kontraindikasi = forms.CharField(label='Kontraindikasi', required= False, max_length=100)
    bentuk_kesediaan = forms.CharField(label='Bentuk Kesediaan', max_length=100)
    


    
    