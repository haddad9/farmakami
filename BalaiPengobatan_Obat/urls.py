from django.urls import path, include
from django.contrib import admin
from .views import bpcreate, bplist, obatcreate, obatlist, updatebp, updateobat, deletebp, deleteobat

app_name = "balaipengobatan"
urlpatterns = [
    path('createBalaiPengobatan', bpcreate, name="bpcreate"),
    path('listBalaiPengobatan', bplist, name="bplist"),
    path('updateBalaiPengobatan/<str:id_balai>', updatebp, name="updatebp"),
    path('deleteBalaiPengobatan/<str:id_balai>', deletebp, name="deletebp"),
    path('createObat', obatcreate, name="obatcreate"),
    path('listObat', obatlist, name="obatlist"),
    path('updateObat/<str:id_obat>/<str:id_produk>/<str:id_merk_obat>', updateobat, name="updateobat"),
    path('deleteObat/<str:id_obat>/<str:id_produk>', deleteobat, name="deleteobat"),
]