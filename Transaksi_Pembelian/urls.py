"""TK4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import createTransaksiPembelian, formCreateTransaksiPembelian, updateTransaksiPembelian, formUpdateTransaksiPembelian, deleteTransaksiPembelian, readTransaksiPembelian

urlpatterns = [
    path('formCT/', formCreateTransaksiPembelian, name='formCreateTP'),
    path('createTP/', createTransaksiPembelian, name='CreateTP'),
    path('formUT/<str:id_transaksi_pembelian>/', formUpdateTransaksiPembelian, name='formUpdateTP'),
    path('updateTP/', updateTransaksiPembelian, name='UpdateTP'),
    path('delTP/<str:id_transaksi_pembelian>/', deleteTransaksiPembelian, name='DeleteTP'),
    path('readTP/', readTransaksiPembelian, name='ReadTP'),
]
