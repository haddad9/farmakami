from django.apps import AppConfig


class TransaksiPembelianConfig(AppConfig):
    name = 'Transaksi_Pembelian'
