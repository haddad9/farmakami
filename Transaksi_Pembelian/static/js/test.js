$(document).ready(function() {
    $(".tombol").click(function(){
        var q = $("#input").val();
        console.log(q)
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
            dataType: "json",
            type: "GET",
            success: function(data) {
                $('#konten').html('')
                    var count = 0;
                    for (var i = 0; i < data.items.length; i++) {
                        var result = '';
                        var isi = '';
                        try {
                            isi = "<img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>";
                        } catch (error) {
                            isi = "undefined";
                        }
                        try {
                            result += "<tr> <th scope='row' class='align-middle text-center'>" + (count + 1) + "</th>" +
                            "<td>" + isi + "</td>" +
                                "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                                "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                                "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                                "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td> </tr>"
                                $('#konten').append(result);
                            count++;
                        } catch (error) {
                            // hehe diemin aja hehe                            
                        }
                    }
            },
            error: function(error) {
                alert("Books not found");
            },
        })
        
    });
});