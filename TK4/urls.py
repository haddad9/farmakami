"""TK4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('login.urls')),
    path('', include('Transaksi_Pembelian.urls')),
    path('', include(('ListProdukDibeli_PengantaranFarmasi.urls','produk_dibeli'), namespace='produk_dibeli')),
    path('', include(('ListProdukDibeli_PengantaranFarmasi.urls', 'pengantaran_farmasi'), namespace='pengantaran_farmasi')),
    path('balaipengobatan/', include(('BalaiPengobatan_Obat.urls', 'balaipengobatan'), namespace='balaipengobatan')),
    path('obat/', include(('BalaiPengobatan_Obat.urls', 'obat'), namespace='obat')),
    path('apotek/', include('apotek.urls')),
    path('produkApotek/', include('produkApotek.urls')),
    path('', include('TransaksiResep_AlatMedis.urls'))
]
