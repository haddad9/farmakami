"""TK4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import viewsTransaksiResep, formUpdateTransaksiResep, updateTransaksiResep, deleteTransaksiResep, formCreateTransaksiResep, createTransaksiResep, viewsAlatMedis, formUpdateAlatMedis, updateAlatMedis, deleteAlatMedis, formCreateAlatMedis, createAlatMedis

#url for app
urlpatterns = [
	path('read_transaksi_resep/', viewsTransaksiResep, name='Transaksi Resep'),
    path('form_update_transaksi_resep/<str:no_resep>/', formUpdateTransaksiResep, name='Form Update Transaksi Resep'),
    path('update_transaksi_resep/', updateTransaksiResep, name='Update Transaksi Resep'),
    path('delete_transaksi_resep/<str:no_resep>/', deleteTransaksiResep, name='Delete Transaksi Resep'),
    path('form_create_transaksi_resep/', formCreateTransaksiResep, name='Form Create Transaksi Resep'),
    path('create_transaksi_resep/', createTransaksiResep, name='Create Transaksi Resep'),
    path('read_alat_medis/', viewsAlatMedis, name='Alat Medis'),
    path('form_update_alat_medis/<str:id_alat_medis>/', formUpdateAlatMedis, name='Form Update Alat Medis'),
    path('update_alat_medis/', updateAlatMedis, name='Update Alat Medis'),
    path('delete_alat_medis/<str:id_alat_medis>/', deleteAlatMedis, name='Delete Alat Medis'),
    path('form_create_alat_medis/', formCreateAlatMedis, name='Form Create Alat Medis'),
    path('create_alat_medis/', createAlatMedis, name='Create Alat Medis')
]