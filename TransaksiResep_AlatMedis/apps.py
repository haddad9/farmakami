from django.apps import AppConfig


class TransaksiresepAlatmedisConfig(AppConfig):
    name = 'TransaksiResep_AlatMedis'
