from django.forms import Form, formset_factory
from django import forms
from collections import namedtuple
from django.db import connection

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def makeChoice(string, start, lst):
    result_string = str(string)
    idx_string = result_string[start:-2]
    temp_tup = (idx_string, idx_string)
    lst.append(temp_tup)

# Transaksi Resep
def transaksiResepChoice():
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_transaksi_pembelian from transaksi_pembelian order by id_transaksi_pembelian::int asc")
    transaksi_pembelian = namedtuplefetchall(cursor)
    cursor.execute("select email from admin_apotek order by email asc")
    email_apotek = namedtuplefetchall(cursor)
    transaksi_pembelian_choice = []
    email_apotek_choice = []
    for i in transaksi_pembelian:
        makeChoice(i, 31, transaksi_pembelian_choice)

    for j in email_apotek:
        makeChoice(j, 14, email_apotek_choice)

    argument = [
        transaksi_pembelian_choice,
        email_apotek_choice
    ]
    return argument

choicesTransaksiResep = transaksiResepChoice()

class UpdateTransaksiResepForm(forms.Form):
    no_resep = forms.CharField(max_length=20, label='No Resep', widget=forms.TextInput(attrs={'readonly':'readonly'}))
    nama_dokter = forms.CharField(max_length=50, label='Nama Dokter', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    nama_pasien = forms.CharField(max_length=50, label='Nama Pasien', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    isi_resep = forms.CharField(label='Isi Resep', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    unggahan_resep = forms.FileField(label='Unggahan Resep', required=False)
    tanggal_resep = forms.DateField(label='Tanggal', widget=forms.TextInput(attrs={'placeholder':'YYYY-MM-DD *Required'}), required=True)
    status_validasi = forms.CharField(max_length=20, label='Status', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    no_telepon = forms.CharField(max_length=20, label='No Telepon', widget=forms.TextInput(attrs={'placeholder':'XXX-XXX-XXXX'}), required=False)
    id_transaksi = forms.CharField(label='ID Transaksi', widget=forms.Select(choices=choicesTransaksiResep[0]))
    email_apoteker = forms.CharField(label='Email Apoteker', widget=forms.Select(choices=choicesTransaksiResep[1]))

class CreateTransaksiResepForm(forms.Form):
    nama_dokter = forms.CharField(max_length=50, label='Nama Dokter', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    nama_pasien = forms.CharField(max_length=50, label='Nama Pasien', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    isi_resep = forms.CharField(label='Isi Resep', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    unggahan_resep = forms.FileField(label='Unggahan Resep', required=False)
    tanggal_resep = forms.DateField(label='Tanggal', widget=forms.TextInput(attrs={'placeholder':'YYYY-MM-DD *Required'}), required=True)
    no_telepon = forms.CharField(max_length=20, label='No Telepon', widget=forms.TextInput(attrs={'placeholder':'XXX-XXX-XXXX'}), required=False)
    id_transaksi = forms.CharField(label='ID Transaksi', widget=forms.Select(choices=choicesTransaksiResep[0]))

# Alat Medis
class UpdateAlatMedisForm(forms.Form):
    id_alat_medis = forms.CharField(max_length=10, label='ID Alat Medis', widget=forms.TextInput(attrs={'readonly':'readonly'}))
    nama_alat_medis = forms.CharField(max_length=50, label='Nama Alat Medis', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    deskripsi_alat = forms.CharField(label='Deskripsi Alat', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    jenis_penggunaan = forms.CharField(max_length=20, label='Jenis Penggunaan', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    id_produk =  forms.CharField(max_length=10, label='ID Produk', widget=forms.TextInput(attrs={'readonly':'readonly'}))

class CreateAlatMedisForm(forms.Form):
    nama_alat_medis = forms.CharField(max_length=50, label='Nama Alat Medis', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    deskripsi_alat = forms.CharField(label='Deskripsi Alat', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    jenis_penggunaan = forms.CharField(max_length=20, label='Jenis Penggunaan', required=True, widget=forms.TextInput(attrs={'placeholder':'*Required'}))