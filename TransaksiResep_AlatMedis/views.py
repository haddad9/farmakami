# Create your views here.
from django.shortcuts import render, redirect
from .forms import UpdateTransaksiResepForm, CreateTransaksiResepForm, UpdateAlatMedisForm, CreateAlatMedisForm
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from collections import namedtuple
from django.db import connection

response = {}

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def max_index(id, relation):
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select "+ id + " from " + relation + " order by " + id +"::int")
    result = namedtuplefetchall(cursor)
    idx = []
    for a in result:
        result_string = str(a).split('=')
        print(result_string)
        idx_string = result_string[1][1:-2]
        idx.append(int(idx_string))
    cursor.close()
    return idx[-1]

# Transaksi Resep
def viewsTransaksiResep(request, message=""):
    cursor = connection.cursor()
    cursor.execute("set search_path to public")
    try:
        Role = request.session['role']
    except:
        return redirect('/')
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from transaksi_dengan_resep order by no_resep asc")
    hasil = namedtuplefetchall(cursor)
    roleAdmin = False
    roleCS = False

    if Role == "Admin":
        roleAdmin = True
    elif Role == "CS":
        roleCS = True

    argument = {
        'hasil' : hasil,
        'roleAdmin' : roleAdmin,
        'roleCS' : roleCS,
        'message' : message
    }
    cursor.close()
    return render(request, 'transaksiResep.html', argument)

def formUpdateTransaksiResep(request, no_resep, success=True, message=""):
    cursor = connection.cursor()
    no_resep = str(no_resep)
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from transaksi_dengan_resep where no_resep ='"+ no_resep +"'")
    hasil = namedtuplefetchall(cursor)
    nama_dokter = hasil[0].nama_dokter
    nama_pasien = hasil[0].nama_pasien
    isi_resep = hasil[0].isi_resep
    unggahan_resep = hasil[0].unggahan_resep
    tanggal_resep = hasil[0].tanggal_resep
    status_validasi = hasil[0].status_validasi
    no_telepon = hasil[0].no_telepon
    id_transaksi = hasil[0].id_transaksi_pembelian
    email_apoteker = hasil[0].email_apoteker
    
    formUpdate = UpdateTransaksiResepForm(initial={'no_resep': no_resep, 'nama_dokter': nama_dokter, 'nama_pasien': nama_pasien, 'isi_resep': isi_resep, 'unggahan_resep': unggahan_resep, 'tanggal_resep': tanggal_resep, 'status_validasi': status_validasi,'no_telepon': no_telepon, 'id_transaksi': id_transaksi, 'email_apoteker': email_apoteker})

    argument = {
        'form' : formUpdate,
        'message' : message
    }
    cursor.close()
    return render(request, 'updateTransaksiResep.html', argument)

def updateTransaksiResep(request):
    no_resep = request.POST['no_resep']
    nama_dokter = request.POST['nama_dokter']
    nama_pasien = request.POST['nama_pasien']
    isi_resep = request.POST['isi_resep']
    unggahan_resep = request.POST['unggahan_resep']
    tanggal_resep = request.POST['tanggal_resep']
    status_validasi = request.POST['status_validasi']
    no_telepon = request.POST['no_telepon']
    id_transaksi = request.POST['id_transaksi']
    email_apoteker = request.POST['email_apoteker']

    testTGL_RSP = tanggal_resep.split("-")
    if(len(testTGL_RSP)!=3):
        return formUpdateTransaksiResep(request, no_resep, True,"Format date yang diberikan tidak sesuai")
    elif(len(testTGL_RSP[0])!=4):
        return formUpdateTransaksiResep(request, no_resep, True,"Format date yang diberikan tidak sesuai")
    elif(len(testTGL_RSP[1])!=2):
        return formUpdateTransaksiResep(request, no_resep, True,"Format date yang diberikan tidak sesuai")
    elif(len(testTGL_RSP[2])!=2):
        return formUpdateTransaksiResep(request, no_resep, True,"Format date yang diberikan tidak sesuai")

    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    
    cursor.execute("update transaksi_dengan_resep set nama_dokter = '" + nama_dokter + "', nama_pasien = '" + nama_pasien 
    +  "', isi_resep = '" + isi_resep + "', unggahan_resep = '" + unggahan_resep + 
    "', tanggal_resep = '" + tanggal_resep + "', status_validasi = '" + status_validasi + 
    "', no_telepon = '" + no_telepon + "', id_transaksi_pembelian = '" + id_transaksi + "', email_apoteker = '" + email_apoteker + 
    "' where no_resep = '"+ no_resep +"'")

    message = "Data berhasil di update"
    return viewsTransaksiResep(request, message)

def deleteTransaksiResep(request, no_resep):
    cursor = connection.cursor()
    no_resep = str(no_resep)

    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from transaksi_dengan_resep where no_resep ='"+ no_resep +"'")

    message = "Data berhasil dihapus"
    return viewsTransaksiResep(request, message)

def formCreateTransaksiResep(request, message=""):
    formCreate = CreateTransaksiResepForm()

    argument = {
        'form' : formCreate,
        'message' : message
    }
    return render(request, 'createTransaksiResep.html', argument)

def createTransaksiResep(request):
    nama_dokter = request.POST['nama_dokter']
    nama_pasien = request.POST['nama_pasien']
    isi_resep = request.POST['isi_resep']
    unggahan_resep = request.POST['unggahan_resep']
    tanggal_resep = request.POST['tanggal_resep']
    status_validasi = "Wait"
    no_telepon = request.POST['no_telepon']
    id_transaksi = request.POST['id_transaksi']

    testTGL_RSP = tanggal_resep.split("-")
    if(len(testTGL_RSP)!=3):
        return formCreateTransaksiResep(request, "Format date yang diberikan tidak sesuai")
    elif(len(testTGL_RSP[0])!=4):
        return formCreateTransaksiResep(request, "Format date yang diberikan tidak sesuai")
    elif(len(testTGL_RSP[1])!=2):
        return formCreateTransaksiResep(request, "Format date yang diberikan tidak sesuai")
    elif(len(testTGL_RSP[2])!=2):
        return formCreateTransaksiResep(request, "Format date yang diberikan tidak sesuai")

    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    no_resep = max_index("no_resep", "transaksi_dengan_resep") + 1
    norsp = str(no_resep)
    cursor.execute("insert into transaksi_dengan_resep (no_resep, nama_dokter, nama_pasien, isi_resep, unggahan_resep, tanggal_resep, status_validasi, no_telepon, id_transaksi_pembelian, email_apoteker) values ('"
    + norsp +"','"+ nama_dokter +"','"+ nama_pasien +"','"+ isi_resep +"','"+ unggahan_resep +"','"+ tanggal_resep +"','"+ status_validasi +"','"+ no_telepon +"','"+ id_transaksi +"', NULL)")
    
    message = "Data berhasil dibuat"
    return viewsTransaksiResep(request, message)

# Alat Medis
def viewsAlatMedis(request, message=""):
    cursor = connection.cursor()
    cursor.execute("set search_path to public")
    try:
        Role = request.session['role']
    except:
        return redirect('/')
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from alat_medis order by id_alat_medis asc")
    hasil = namedtuplefetchall(cursor)
    roleAdmin = False
    roleCS = False

    if Role == "Admin":
        roleAdmin = True
    elif Role == "CS":
        roleCS = True

    argument = {
        'hasil' : hasil,
        'roleAdmin' : roleAdmin,
        'roleCS' : roleCS,
        'message' : message
    }
    cursor.close()
    return render(request, 'alatMedis.html', argument)

def formUpdateAlatMedis(request, id_alat_medis):
    cursor = connection.cursor()
    id_alat_medis = str(id_alat_medis)
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from alat_medis where id_alat_medis ='"+ id_alat_medis + "'")
    hasil= namedtuplefetchall(cursor)
    nama_alat_medis = hasil[0].nama_alat_medis
    deskripsi_alat = hasil[0].deskripsi_alat
    jenis_penggunaan = hasil[0].jenis_penggunaan
    id_produk = hasil[0].id_produk
    
    formUpdate = UpdateAlatMedisForm(initial={'id_alat_medis': id_alat_medis, 'nama_alat_medis': nama_alat_medis, 'deskripsi_alat': deskripsi_alat, 'jenis_penggunaan': jenis_penggunaan, 'id_produk': id_produk})

    argument = {
        'form' : formUpdate,
    }
    cursor.close()
    return render(request, 'updateAlatMedis.html', argument)

def updateAlatMedis(request):
    id_alat_medis = request.POST['id_alat_medis']
    nama_alat_medis = request.POST['nama_alat_medis']
    deskripsi_alat = request.POST['deskripsi_alat']
    jenis_penggunaan = request.POST['jenis_penggunaan']
    id_produk =  request.POST['id_produk']

    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    
    cursor.execute("update alat_medis set nama_alat_medis = '" + nama_alat_medis + "', deskripsi_alat = '" + deskripsi_alat
    + "', jenis_penggunaan = '"+ jenis_penggunaan + "' where id_alat_medis = '"+ id_alat_medis + "' AND id_produk = '" + id_produk + "'")

    message = "Data berhasil di update"
    return viewsAlatMedis(request, message)

def deleteAlatMedis(request, id_alat_medis):
    cursor = connection.cursor()
    id_alat_medis = str(id_alat_medis)
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_produk as id from alat_medis where id_alat_medis ='"+ id_alat_medis + "'")
    id_produk = namedtuplefetchall(cursor)
    id_produk = id_produk[0].id
    cursor.execute("delete from alat_medis where id_alat_medis ='"+ id_alat_medis + "'")
    cursor.execute("delete from produk where id_produk ='"+ id_produk + "'")
    cursor.execute("select * from alat_medis order by id_alat_medis asc")
    hasil = namedtuplefetchall(cursor)

    message = "Data berhasil dihapus"
    return viewsAlatMedis(request, message)

def formCreateAlatMedis(request):
    formCreate = CreateAlatMedisForm()

    argument = {
        'form' : formCreate,
    }
    return render(request, 'createAlatMedis.html', argument)

def createAlatMedis(request):
    nama_alat_medis = request.POST['nama_alat_medis']
    deskripsi_alat = request.POST['deskripsi_alat']
    jenis_penggunaan = request.POST['jenis_penggunaan']
    
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    id_alat_medis = max_index("id_alat_medis", "alat_medis") + 1
    id_produk = max_index("id_produk", "produk") + 1
    idalt = str(id_alat_medis)
    idprdk = str(id_produk)
    cursor.execute("insert into produk(id_produk) values ('" + idprdk +"')")
    cursor.execute("insert into alat_medis(id_alat_medis, nama_alat_medis, deskripsi_alat, jenis_penggunaan, id_produk) values ('"
    + idalt +"','"+ nama_alat_medis +"','"+ deskripsi_alat +"','"+ jenis_penggunaan +"','"+ idprdk +"')")
    
    message = "Data berhasil dibuat"
    return viewsAlatMedis(request, message)