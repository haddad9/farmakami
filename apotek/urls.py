from django.urls import path, include
from django.contrib import admin
from .views import createApotek, tableApotek, updateApotek, deleteApotek

app_name = "apotek"
urlpatterns = [
    path('createApotek', createApotek, name="createApotek"),
    path('tableApotek', tableApotek, name="tableApotek"),
    path('updateApotek/<str:id_apotek>', updateApotek, name="updateApotek"),
    path('deleteApotek/<str:id_apotek>', deleteApotek, name="deleteApotek"),
]